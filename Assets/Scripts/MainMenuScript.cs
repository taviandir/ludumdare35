﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour 
{
	public GameObject mainMenuPanel;
	public GameObject instructionsPanel;
	
	public void StartNewGame()
	{
	    Debug.Log("loading scene main");
        SceneManager.LoadScene("level2");
	}

    public void StartTutorial()
    {
        SceneManager.LoadScene(1);
    }
	
	public void ShowInstructions()
	{
		mainMenuPanel.SetActive(false);
		instructionsPanel.SetActive(true);
	}
	
	public void BackToMainMenu()
	{
		mainMenuPanel.SetActive(true);
		instructionsPanel.SetActive(false);
	}
	
	public void QuitApplication()
	{
		Application.Quit();
	}
}
