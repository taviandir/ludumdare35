﻿using UnityEngine;
using System.Collections;

public class ControllerPlayer : ControllerActor {
    public override void Move(Vector3 displacement, bool standingOnPlatform = false)
    {
        Move(displacement, Vector2.zero, standingOnPlatform);
    }

    public void Move(Vector3 displacement, Vector2 input, bool standingOnPlatform = false)
    {
        base.UpdateAndReset(displacement, standingOnPlatform);
        playerInput = input;

        if (displacement.y < 0)
        {
            DescendSlope(ref displacement);
        }

        HorizontalCollisions(ref displacement);

        if (displacement.y != 0)
        {
            VerticalCollisions(ref displacement);
        }
        transform.Translate(displacement);
    }
}