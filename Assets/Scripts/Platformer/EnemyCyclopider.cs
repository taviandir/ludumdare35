﻿using UnityEngine;
using System.Collections;

public class EnemyCyclopider : Enemy {

    void Update()
    {
        UpdateBase();

        if (!health.isDead)
        {
            if (behaviour == Behaviour.Patrol && !isTurning) { 
                Patrol(velocity * Time.deltaTime);
            }
            float targetVelocityX = moveSpeed * targetDirectionX;
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, accelerationTime);
            controller.Move(velocity * Time.deltaTime);
            Gravity();
            UpdateAnimator(targetDirectionX);
        }
    }

    public override void PlayDeathSound()
    {
        SoundManager.Instance.PlaySpiderDeathSound();
    }
}
