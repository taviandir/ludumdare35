﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class CheckPoint : MonoBehaviour {

    public float fadeTime = 2f;
    public float addToCountdown = 15f;

    private float fadeSmoothing;

    void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.transform.GetComponent<PlatformerPlayer>();
        if (player != null)
        {
            GameManager.Instance.countdown += addToCountdown;
            player.activeCheckpoint = transform.position;
            
            transform.GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<BoxCollider2D>().enabled = false;
            transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            SoundManager.Instance.PlayCheckpointSound();
        }
    }
    void Fade()
    {
        transform.GetComponent<SpriteRenderer>().material.color -= new Color(0, 0, 0, Mathf.SmoothDamp(0, 1, ref fadeSmoothing, 0.1f));
    }
	
}
