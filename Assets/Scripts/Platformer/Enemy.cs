﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(ControllerEnemy))]
[RequireComponent(typeof(EnemyHealth))]
public class Enemy : Moveable {
    // Behaviour variables
    public float ledgeHeight = 0.1f;
    public float accelerationTime = .2f;
    public float detectRange;

    public float attackModeDuration;

    // movement behaviour
    protected float prevTargetDirectionX = -1;
    protected float targetDirectionX = 1;

    // tells the enemy how long it should pause at ledges before turning
    public float patrolPause;
    protected Behaviour behaviour;
    protected enum Behaviour { Patrol, Attack }
    protected bool isTurning;

    protected Transform playerTransform;

    
    override protected void Start() {
        base.Start();

        health = GetComponent<EnemyHealth>();
        controller = GetComponent<ControllerEnemy>();
    }
    
    void Update()
    {
        UpdateBase();
        if (health.isDead)
            return;
        if (behaviour == Behaviour.Patrol && !isTurning)
            Patrol(velocity * Time.deltaTime);

        float targetVelocityX = moveSpeed * targetDirectionX;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, accelerationTime);
        controller.Move(velocity * Time.deltaTime);
        Gravity();
        UpdateAnimator(targetDirectionX);
    }

    public void UpdateBase()
    {
        if (controller.collisions.above || controller.collisions.below)
        {
            velocity.y = 0;
        }
    }

    public bool IsNotCompletelyGrounded(Vector2 displacement)
    {
        float directionX = targetDirectionX;
        Vector2 rayOrigin = directionX == -1 ? controller.raycastOrigins.bottomLeft : controller.raycastOrigins.bottomRight;
        rayOrigin.x += displacement.x;
        RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.down, Mathf.Infinity, controller.collisionMask);

        return !hit || hit.distance > ledgeHeight;
    }

    public bool IsInObstacle(Vector2 displacement)
    {
        //targetDirectionX
        float directionX = Mathf.Sign(displacement.x);
        Vector2 rayOrigin = directionX == -1 ? controller.raycastOrigins.bottomLeft : controller.raycastOrigins.bottomRight;
        rayOrigin.x += displacement.x;
        RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, Mathf.Infinity, controller.collisionMask);

        return hit && hit.distance < Mathf.Abs(displacement.x);
    }

    // Behaviour implementations
    public void Patrol(Vector2 displacement)
    {
        if (IsNotCompletelyGrounded(displacement) || IsInObstacle(displacement)) {
            //print("OBSTACLE:" + IsInObstacle(displacement));
            //print("GROUNDED:" + IsNotCompletelyGrounded(displacement));
            isTurning = true;
            velocity.x = 0;
            updateTargetDirectionX(0);
            Invoke("PatrolTurn", patrolPause);
            
        }
    }

    

    public virtual void DetectPlayer()
    {
        float directionX = targetDirectionX;
        float rayLength = detectRange;
        var raycastOrigins = controller.raycastOrigins;
        var horizontalRayCount = controller.horizontalRayCount;
        var horizontalRaySpacing = controller.horizontalRaySpacing;

        for (int i = 0; i < horizontalRayCount; i++)
        {
            //Check if Player front
            Vector2 rayOriginFront = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
            rayOriginFront += Vector2.up * (horizontalRaySpacing * i);
            RaycastHit2D hit = Physics2D.Raycast(rayOriginFront, Vector2.right * directionX, rayLength);

            Debug.DrawRay(rayOriginFront, Vector2.right * directionX * rayLength, Color.red);

            if (hit)
            {
                var targetTransform = hit.collider.transform;
                if (targetTransform.GetComponent<PlatformerPlayer>() != null) {
                    playerTransform = targetTransform;
                }
            }
        }
    }

    public void PatrolTurn()
    {
        updateTargetDirectionX(-prevTargetDirectionX);
        isTurning = false;
    }

    public void updateTargetDirectionX(float newTargetDirectionX)
    {
        prevTargetDirectionX = targetDirectionX;
        targetDirectionX = newTargetDirectionX;
    }

    public virtual void PlayDeathSound()
    {
    }
}
