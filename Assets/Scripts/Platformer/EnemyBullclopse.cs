﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ControllerEnemy))]
[RequireComponent(typeof(EnemyHealth))]
public class EnemyBullclopse : Enemy
{
    public float chargeUpTime = 1f;
    public float chargeRange = 4f;
    public float chargeSpeed = 10f;
    private bool isChargingUp;
    private bool isCharging;
    private float defaultSpeed;
    public LayerMask player;
    private bool playerDetected;
    private float timeSincePlayerDetected;

    Vector2 chargeStartPosition;
    // Use this for initialization
    override protected void Start()
    {
        base.Start();
        defaultSpeed = moveSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateBase();
        if (health.isDead)
            return;

        // if it is not charging or charging up
        if (!isCharging && !isChargingUp)
        {
            if (timeSincePlayerDetected > attackModeDuration)
            {
                behaviour = Behaviour.Patrol;

            }
            if (behaviour == Behaviour.Patrol && !isTurning)
                Patrol(velocity * Time.deltaTime);

            if (playerDetected)
                behaviour = Behaviour.Attack;
                
            if (behaviour == Behaviour.Attack)
                Attack(); 
        }

        if (isCharging)
        {
            velocity.x = moveSpeed * targetDirectionX;
        }
        else
        {
            float targetVelocityX = moveSpeed * targetDirectionX;
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, accelerationTime);
        }

        if (!isChargingUp) { 
            controller.Move(velocity * Time.deltaTime);
        }
        DetectPlayer();
        Gravity();
        UpdateAnimator(targetDirectionX);
        timeSincePlayerDetected += Time.deltaTime;
    }

    override public void DetectPlayer()
    {
        float directionX = targetDirectionX;
        float rayLength = chargeRange-1;
        var raycastOrigins = controller.raycastOrigins;
        var horizontalRayCount = controller.horizontalRayCount;
        var horizontalRaySpacing = controller.horizontalRaySpacing;

        for (int i = 0; i < horizontalRayCount; i++)
        {
            //Check if Player front
            Vector2 rayOriginFront = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
            rayOriginFront += Vector2.up * (horizontalRaySpacing * i);
            RaycastHit2D hit = Physics2D.Raycast(rayOriginFront, Vector2.right * directionX, rayLength, player);

            Debug.DrawRay(rayOriginFront, Vector2.right * directionX * rayLength, Color.red);

            if (hit)
            {
                var targetTransform = hit.collider.transform;
                
                if (targetTransform.GetComponent<PlatformerPlayer>() != null)
                {
                    playerTransform = targetTransform;
                    playerDetected = true;
                    timeSincePlayerDetected = 0.0f;
                    break;
                }
                else
                {
                    playerDetected = false;
                }

            }
            else
            {
                playerDetected = false;
            }

            //// Check if Player behind
            //Vector2 rayOriginBack = (directionX == 1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
            //rayOriginBack += Vector2.up * (horizontalRaySpacing * i);
            //hit = Physics2D.Raycast(rayOriginBack, Vector2.right * directionX, rayLength);

            //Debug.DrawRay(rayOriginBack, Vector2.right * directionX * rayLength, Color.red);

            //if (hit)
            //{

            //}
        }
    }

    public void Attack()
    {
        if (!isChargingUp && !isCharging && playerDetected && behaviour == Behaviour.Attack) {
            isChargingUp = true;
            targetDirectionX = Mathf.Sign(playerTransform.position.x - transform.position.x);
            animator.SetTrigger("ChargingUp");
            Invoke("Charge", chargeUpTime);
        }
    }

    public void Charge()
    {
        float diffPlayerAndEnemyPositionX = playerTransform.position.x - transform.position.x;
        isChargingUp = false;

        bool debuggingthisfunction = false;
        if (debuggingthisfunction)
        { 
            print("IS GROUNDED POSITION" + !IsNotCompletelyGrounded(new Vector2(diffPlayerAndEnemyPositionX, 0)));
            print("IS WITHIN DISTANCE" + (Mathf.Abs(diffPlayerAndEnemyPositionX) <= chargeRange));
            print("IS IN CORRECT DIRECTION" + (diffPlayerAndEnemyPositionX * targetDirectionX > 0));
        }

        // if the player is over ledge, in range and in correct direction of charge up --> CHARGE
        if (!IsNotCompletelyGrounded(new Vector2(chargeRange * targetDirectionX, 0))) // && diffPlayerAndEnemyPosition * targetDirectionX > 0
        {
            chargeStartPosition = transform.position;
            isCharging = true;
            animator.SetTrigger("DoAttack");

            StartCoroutine(ChargeUntilWallOrChargeRange());
        }
        else
        {
            animator.SetTrigger("CancelChargingUp");
        }
        // if the player is over ledge and in range --> START NEW ATTACK
        //else if (Mathf.Abs(diffPlayerAndEnemyPositionX) < chargeRange && !IsNotCompletelyGrounded(new Vector2(diffPlayerAndEnemyPositionX, 0)) && playerDetected)
        //{
        //    Attack();
        //}
        //Invoke("ExitAttackMode", attackModeDuration);
    }

    public IEnumerator ChargeUntilWallOrChargeRange()
    {
        SoundManager.Instance.PlayBullChargeSound();
        moveSpeed = chargeSpeed;
        // || !IsNotCompletelyGrounded(new Vector2(moveSpeed*targetDirectionX*Time.deltaTime,0))
        yield return new WaitUntil(() => chargeRange <= Mathf.Abs(transform.position.x - chargeStartPosition.x) || 
                                            IsInObstacle(new Vector2(0.2f * Mathf.Sign(velocity.x), 0)) //||
                                            //IsNotCompletelyGrounded(new Vector2(velocity.x*Time.deltaTime, 0))
                                            );
        moveSpeed = defaultSpeed;
        isCharging = false;
    }

    public override void PlayDeathSound()
    {
        SoundManager.Instance.PlayBullDeathSound();
    }
}
