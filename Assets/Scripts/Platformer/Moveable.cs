﻿using UnityEngine;
using System.Collections;

public class Moveable : MonoBehaviour {

    public float moveSpeed = 4f;

    
    protected Vector3 velocity;
    protected float velocityXSmoothing;
    protected ControllerActor controller;
    protected PhysicsManager physicsManager;
    public float dragCoefficient = 1f;
    protected float gravity { get { return PhysicsManager.Instance.gravity; } }

    protected HealthBase health;
    // Animations
    public SpriteRenderer sr;
    public Animator animator;

    // Use this for initialization
    protected virtual void Start () {
        physicsManager = PhysicsManager.Instance;
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
    }

    protected void StopMovement()
    {
        velocity.x = 0;
        velocity.y = 0;
    }

    protected void Gravity()
    {
        velocity.y += gravity * Time.deltaTime;
    }

    protected void AirDrag()
    {
        float airDrag = 0;
        if (!controller.collisions.below)
            airDrag = velocity.y * dragCoefficient * Time.deltaTime; // check if v2 better later
        velocity.y += -Mathf.Sign(velocity.y) * airDrag;
    }

    public virtual void Death()
    {
        // ToDo : send death event to animator
        animator.SetTrigger("DoDeath");
    }

    public void FlipSprite(float targetDirectionX)
    {
        // flip sprite depending on direction
        sr.flipX = targetDirectionX < 0 ? true : targetDirectionX > 0 ? false : sr.flipX;
        //sr.flipX = controller.collisions.faceDir == -1;
    }
    public void UpdateAnimator(float targetDirectionX)
    {
        FlipSprite(targetDirectionX);
        animator.SetBool("isGrounded", controller.collisions.below);
        animator.SetFloat("velocityY", velocity.y);
        animator.SetFloat("velocityX", Mathf.Abs(targetDirectionX));
    }
}
