﻿/*

Based on the platform controller tutorial series by Sebastian Lague
URL: https://www.youtube.com/watch?v=MbWK8bCAU2w&list=PLFt_AvWsXl0f0hqURlhyIoAabKPgRsqjz

*/

using UnityEngine;

[RequireComponent (typeof (ControllerPlayer))]
[RequireComponent(typeof(PlayerHealth))]
public class PlatformerPlayer : Moveable
{
    public bool hasControl = true;
    public bool wallSlidingEnabled = false;
    public bool doubleJumpEnabled = false;
	
	public float minJumpHeight = 1f;
	public float maxJumpHeight = 4f;
	public float timeToJumpApex = .4f;
	public Vector2 wallJumpClimb = new Vector2(7.5f, 16f);
	public Vector2 wallJumpOff = new Vector2(8.5f, 7f);
	public Vector2 wallLeap = new Vector2(18f, 17f);
	public float wallStickTime = 0.25f;
    public float bounceOnEnemyMultiplier = 0.75f;
    
    public float shapeShiftDuration = 0.5f;
    public float icicleSpeedBoost = 10;
    public Vector2 activeCheckpoint;
    public GameObject trailRenderer;
    
	private float accelerationTimeAirborne = .2f;
	private float accelerationTimeGrounded = .1f;
    private float timeToWallUnstick;
    private float wallSlideSpeedMax = 2.5f;
    
	private float maxJumpSpeed;
	private float minJumpSpeed;
    private bool hasDoubleJumped = false;

    private bool isGrounded = false;
    
    // Shape Variables
    [HideInInspector]
    public Shape currentShape;
    private bool isShapeShifting;
    private float shapeShiftTime;
    public enum Shape { BaseWater, Icicle }

    override protected void Start() 
	{
        base.Start();
        controller = GetComponent<ControllerPlayer>();
        health = GetComponent<PlayerHealth>();
        // set default check point to start of map
        activeCheckpoint = transform.position;
        currentShape = Shape.BaseWater;
        isShapeShifting = false;

        // set gravity from jumpheight and jumpapex
        var g = -(2 * maxJumpHeight) / Mathf.Pow (timeToJumpApex, 2);
		maxJumpSpeed = Mathf.Abs(g) * timeToJumpApex;
		minJumpSpeed = Mathf.Sqrt(2*Mathf.Abs(g) * minJumpHeight);
        physicsManager.gravity = g;
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        var enemy = other.GetComponent<Enemy>();
        if (enemy != null) {
            if (currentShape == Shape.Icicle)
            {
                ShiftFromIcicle();
                velocity.y = maxJumpSpeed * bounceOnEnemyMultiplier;
                animator.SetTrigger("DoJump");
                enemy.GetComponent<EnemyHealth>().ReceiveDamage(1);
            }
            else
            {
                health.ReceiveDamage(1);
                velocity -= velocity.normalized * 4f;
            }
        }

        // Spike pushback
        var iceSpike = other.GetComponent<IceSpike>();
        
        if (iceSpike)
        {
            velocity = (iceSpike.direction-velocity).normalized * iceSpike.pushBackSpikes;
        }

        var fallingSpike = other.GetComponent<FallingSpike>();
        if (fallingSpike)
        {
            velocity = -velocity.normalized * fallingSpike.pushBackSpikes;
        }
    }


    void ShiftFromIcicle()
    {
        currentShape = Shape.BaseWater;
        Invoke("DisableTrailRenderer", 0.15f); // ToDo : dont like using magical strings, but its good enough for LD
        RaycastHit2D hit = Physics2D.Raycast(new Vector2(sr.bounds.min.x + (sr.bounds.size.x/2.0f), sr.bounds.min.y),
            Vector2.down, 0.1f, 1 << GameLayers.LAYER_OBSTACLE);
        if (hit)
        {
            var dg = hit.collider.GetComponent<DestructibleGround>();
            if (dg)
            {
                dg.RemoveGround();
                animator.SetTrigger("DoJump"); // quickfix to cancel icicle shape
            }
        }
    }

    void Update()
	{
	    if (!hasControl)
            return;
        Vector2 input = Vector2.zero;

        if (currentShape == Shape.BaseWater) {
            input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

            float targetVelocityX = input.x * moveSpeed;
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, controller.collisions.below ? accelerationTimeGrounded : accelerationTimeAirborne);

            Jump(input);
        }
        
        // Shapeshift to Icicle
        if (!controller.collisions.below && Input.GetAxisRaw("Vertical") < 0) {
            if (currentShape == Shape.BaseWater)
            {
                //Debug.Log("ICICLE charge");
                animator.SetTrigger("DoIcicle");
                currentShape = Shape.Icicle;
                isShapeShifting = true;
                shapeShiftTime = 0;
                StopMovement();
                // possibly give a Input Down that boosts speed with smoothing to max speed and then lowered again to terminal velocity
                // implement airDrag to terminal velocity when above terminal velocity
            }
        }


        // Shapeshift back to BaseWater
        if (currentShape == Shape.Icicle && controller.collisions.below)
        {
            ShiftFromIcicle();
        }
        Gravity();

        // While shapeshifting do this
        if (isShapeShifting)
        {
            if (shapeShiftTime >= shapeShiftDuration) {
                // when it quits Icicle transform, give extra speed boost down (make this a nicer smoothing effect from VERY FAST to TERMINAL VELOCITY)
                if (currentShape == Shape.Icicle)
                {
                    velocity.y -= icicleSpeedBoost;
                    trailRenderer.SetActive(true);
                    //animator.SetTrigger("DoIcicleCharge");
                    isShapeShifting = false;
                }
            }
            else { 
                shapeShiftTime += Time.deltaTime;
                StopMovement();
            }
        }
        
        (controller as ControllerPlayer).Move(velocity * Time.deltaTime, input);

        if (controller.collisions.above || controller.collisions.below)
        {
            velocity.y = 0;
        }

        if (!controller.collisions.below && isGrounded)
            isGrounded = false;
        else if (controller.collisions.below && !isGrounded)
        {
            isGrounded = true;
            SoundManager.Instance.PlayPlayerLandSound();
        }
        
        UpdateAnimator(input.x);
    }

    public void Jump(Vector2 input)
    {
        int wallDirX = (controller.collisions.left) ? -1 : 1;
        bool wallSliding = false;
        if (wallSlidingEnabled && (controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0)
        {
            wallSliding = true;
            hasDoubleJumped = false;
            if (velocity.y < -wallSlideSpeedMax)
            {
                velocity.y = -wallSlideSpeedMax;
            }
            if (timeToWallUnstick > 0)
            {
                velocityXSmoothing = 0;
                velocity.x = 0;

                if (input.x != wallDirX && input.x != 0)
                {
                    timeToWallUnstick -= Time.deltaTime;
                }
                else
                {
                    timeToWallUnstick = wallStickTime;
                }
            }
            else
            {
                timeToWallUnstick = wallStickTime;
            }
        }

        if (controller.collisions.below)
            hasDoubleJumped = false;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            // wall jump
            if (wallSlidingEnabled && wallSliding)
            {
                if (wallDirX == input.x)
                {
                    velocity.x = -wallDirX * wallJumpClimb.x;
                    velocity.y = wallJumpClimb.y;
                }
                else if (input.x == 0)
                {
                    velocity.x = -wallDirX * wallJumpOff.x;
                    velocity.y = wallJumpOff.y;
                }
                else
                {
                    velocity.x = -wallDirX * wallLeap.x;
                    velocity.y = wallLeap.y;
                }
            }
            // normal jump
            if (controller.collisions.below)
            {
                velocity.y = maxJumpSpeed;
                animator.SetTrigger("DoJump");
                SoundManager.Instance.PlayPlayerJumpSound();
            }
            // in-air jump
            else if (doubleJumpEnabled && !controller.collisions.above && !hasDoubleJumped)
            {
                hasDoubleJumped = true;
                velocity.y = maxJumpSpeed * 0.75f;
                animator.SetTrigger("DoJump");
            }
        }
        if (Input.GetKeyUp(KeyCode.Space) && !hasDoubleJumped)
        {
            if (velocity.y > minJumpSpeed)
                velocity.y = minJumpSpeed;
        }
    }

    public void ToggleControl(bool value)
    {
        hasControl = value;
    }
    
    public override void Death()
    {
        base.Death(); 
        hasControl = false;
    }

    public void Respawn()
    {
        Respawn(activeCheckpoint);
        hasControl = true;
        animator.SetTrigger("DoRespawn");
    }

    private void Respawn(Vector2 lastCheckPoint)
    {
        transform.position = lastCheckPoint; // ToDo : proper respawn
    }

    private void DisableTrailRenderer()
    {
        trailRenderer.SetActive(false);
        //Debug.Log("disabling trailrenderer");
    }
}