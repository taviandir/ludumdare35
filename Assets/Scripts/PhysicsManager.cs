﻿using UnityEngine;

public class PhysicsManager : MonoBehaviour {

    public float gravity;

    private static PhysicsManager _instance;
    public static PhysicsManager Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        _instance = this;
    }
}