﻿using UnityEngine;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour
{
    public AudioSource musicSource;
    public AudioClip beginningTune;
    public AudioClip runningTune;
    public AudioSource effectsSource;
    public AudioClip breakableGroundDestroyed;
    public AudioClip bullCharge;
    public AudioClip bullDeath;
    public AudioClip checkPoint;
    public AudioClip coinPickup;
    public AudioClip countdown;
    public AudioClip fallingSpikeDestroyed;
    public AudioClip healthPickup;
    public AudioClip playerDeath;
    public AudioClip playerHurt;
    public AudioClip playerJump;
    public AudioClip playerLand;
    public AudioClip spiderDeath;
    public AudioClip timePickup;

    private static SoundManager _instance;

    public static SoundManager Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        _instance = this;
    }

    public void StopCurrentMusic()
    {
        musicSource.Stop();
    }

    public void PlayRunningMusic()
    {
        musicSource.clip = runningTune;
        musicSource.Play();
    }

    public void PlayBreakableGroundDestroyedSound()
    {
        effectsSource.PlayOneShot(breakableGroundDestroyed);
    }

    public void PlayBullChargeSound()
    {
        effectsSource.PlayOneShot(bullCharge);
    }

    public void PlayBullDeathSound()
    {
        effectsSource.PlayOneShot(bullDeath);
    }

    public void PlayCheckpointSound()
    {
        effectsSource.PlayOneShot(checkPoint);
    }

    public void PlayCoinPickupSound()
    {
        effectsSource.PlayOneShot(coinPickup);
    }

    public void PlayCountdownSound()
    {
        effectsSource.PlayOneShot(countdown);
    }

    public void PlayFallingSpikeDestroyedSound()
    {
        effectsSource.PlayOneShot(fallingSpikeDestroyed);
    }

    public void PlayHealthPickupSound()
    {
        effectsSource.PlayOneShot(healthPickup);
    }

    public void PlayPlayerDeathSound()
    {
        effectsSource.PlayOneShot(playerDeath);
    }

    public void PlayPlayerHurtSound()
    {
        effectsSource.PlayOneShot(playerHurt);
    }

    public void PlayPlayerJumpSound()
    {
        effectsSource.PlayOneShot(playerJump);
    }

    public void PlayPlayerLandSound()
    {
        //effectsSource.PlayOneShot(playerLand);
    }

    public void PlaySpiderDeathSound()
    {
        effectsSource.PlayOneShot(spiderDeath);
    }

    public void PlayTimePickupSound()
    {
        effectsSource.PlayOneShot(timePickup);
    }
}
