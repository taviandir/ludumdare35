﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Gameplay;

public class AStarManhattanTileMaker : AStarAlgorithm<TileNode>
{
    public int Multiplier { get; private set; }

    public AStarManhattanTileMaker()
    {
        Multiplier = 1;
    }

    public AStarManhattanTileMaker(int multiplier)
    {
        Multiplier = multiplier;
    }

    protected override double Heuristic(TileNode p1, TileNode p2)
    {
        return (Math.Abs(p1.X - p2.X) + Math.Abs(p1.Y - p2.Y)) * Multiplier;
    }

    protected override IEnumerable<TileNode> ConnectionsOf(TileNode p)
    {
        return p.Connections;
    }

    protected override int CostBetween(TileNode from, TileNode to)
    {
        return 1;
    }
}
