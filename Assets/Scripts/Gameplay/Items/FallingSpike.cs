﻿using UnityEngine;
using System.Collections.Generic;

public class FallingSpike : MonoBehaviour
{
    public ShakeLeftRight shaker;
    public LayerMask collisionMask;
    public float fallWaitDuration = 1.5f;
    public float fallSpeed = 1;
    public int damage = 1;
    public float pushBackSpikes = 1f;

    private bool triggered = false;
    private float timeTriggered;
    private float currentSpeed;

    void FixedUpdate()
    {
        if (!triggered)
        {
            BoxCollider2D collider = (BoxCollider2D)this.GetComponent<BoxCollider2D>();
            var bounds = collider.bounds;
            var bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
            var bottomRight = new Vector2(bounds.max.x, bounds.min.y);

            var hit = Physics2D.Raycast(bottomLeft, Vector2.down, 2f, collisionMask);
            if (hit)
            {
                Triggered();
                //Debug.Log("spike hit L:" + hit.collider.gameObject.name);
            }
            else
            {
                hit = Physics2D.Raycast(bottomRight, Vector2.down, 2f, collisionMask);
                if (hit)
                {
                    Triggered();
                    //Debug.Log("spike hit R:" + hit.collider.gameObject.name);
                }
            }
        }
        else // Triggered
        {
            if (Time.time > timeTriggered + fallWaitDuration)
            {
                shaker.isShaking = false;
                currentSpeed = Mathf.Lerp(currentSpeed, fallSpeed, Time.fixedDeltaTime);
                transform.Translate(new Vector3(0, currentSpeed * -1.0f, 0));
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == GameLayers.LAYER_PLAYER)
        {
            PlayerHealth.Instance.ReceiveDamage(damage);
            SoundManager.Instance.PlayFallingSpikeDestroyedSound();
            Destroy(this.gameObject);
        }
        else if (other.gameObject.layer == GameLayers.LAYER_OBSTACLE)
        {
            Destroy(this.gameObject);
            SoundManager.Instance.PlayFallingSpikeDestroyedSound();
        }
    }

    private void Triggered()
    {
        triggered = true;
        timeTriggered = Time.time;
        shaker.isShaking = true;
    }
}
