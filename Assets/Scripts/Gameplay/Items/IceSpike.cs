﻿using UnityEngine;
using System.Collections.Generic;

public class IceSpike : MonoBehaviour
{
    public int damage = 1;
    public float pushBackSpikes = 3f;
    public Vector3 direction;
    public bool damageInvulnerable = false;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayerHealth.Instance.ReceiveDamage(damage, damageInvulnerable);
        }
    }
}