﻿using UnityEngine;
using System.Collections.Generic;

public class HealthPickup : MonoBehaviour
{
    public int healthBonus = 1;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayerHealth.Instance.GrantHealth(healthBonus);
            SoundManager.Instance.PlayHealthPickupSound();
            Destroy(gameObject);
        }
    }
}
