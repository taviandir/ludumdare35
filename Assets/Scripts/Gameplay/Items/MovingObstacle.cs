﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class MovingObstacle : MonoBehaviour
{
    public Vector3 velocity;
    public bool isAutomatic = false;
    public bool isTriggered = false;
    public bool isFalling = false;
    public bool isLooking = true;
    public bool isReturning = false;
    public int lookingRaycasts = 4;
    public float maxSpeedYDown = 1f;
    public float maxSpeedYUp = 1f;
    public float accelerationTimeDown = 0.05f;
    public float accelerationTimeUp = 0.05f;
    public float fallWaitDuration = 1.5f;
    public float automaticIdleDuration = 2f;
    public float autoTimeOffset = 0f;
    public ShakeLeftRight shaker;

    protected float velocityYSmoothing;
    private float oldTargetY;
    private float timeTriggered;
    private float fallingTargetY;
    private float waitingY;
    private float startingX;
    private float timeIdleStart;

    private void Start()
    {
        waitingY = this.y();
        startingX = this.x();

        // find out where ground begins
        BoxCollider2D collider = (BoxCollider2D) this.GetComponent<BoxCollider2D>();
        var bounds = collider.bounds;
        var bottomCenter = new Vector2(bounds.min.x + (bounds.size.x/2f), bounds.min.y);
        var hit = Physics2D.Raycast(bottomCenter, Vector2.down, 100f, 1 << GameLayers.LAYER_OBSTACLE);
        if (hit)
        {
            float offset = bounds.size.y / 2f;
            float pointY = hit.point.y;
            fallingTargetY = offset + pointY;
        }
        else
            Debug.LogError("MovingObstace: no ground found!");
    }

    void Update()
    {
        if (isFalling)
        {
            float targetDirectionY = -1.0f;
            float targetVelocityY = maxSpeedYDown * targetDirectionY;
            velocity.y = Mathf.SmoothDamp(velocity.y, targetVelocityY, ref velocityYSmoothing, accelerationTimeDown);
            Vector3 displacement = velocity * Time.deltaTime;
            if (displacement.y + transform.position.y <= fallingTargetY)
            {
                transform.position = new Vector3(transform.x(), fallingTargetY);
                isFalling = false;
                isReturning = true;

            }
            else
                transform.position += displacement;
        }
        else if (isReturning)
        {
            float targetDirectionY = 1.0f;
            float targetVelocityY = maxSpeedYUp * targetDirectionY;
            velocity.y = Mathf.SmoothDamp(velocity.y, targetVelocityY, ref velocityYSmoothing, accelerationTimeUp);
            Vector3 displacement = velocity * Time.deltaTime;
            // ToDo : check Y in relation to waitingY
            if (displacement.y + transform.position.y >= waitingY)
            {
                transform.position = new Vector3(transform.x(), waitingY);
                isReturning = false;
                isLooking = true;
                if (isAutomatic)
                    timeIdleStart = Time.time;
            }
            else
                transform.position += displacement;
        }
        else if (isTriggered)
        {
            if (Time.time > timeTriggered + fallWaitDuration)
            {
                // wait time over
                shaker.isShaking = false;
                isFalling = true;
                isTriggered = false;
                transform.position = new Vector3(startingX, transform.position.y);
            }
        }
        else if (isAutomatic && isLooking)
        {
            if (Time.time > timeIdleStart + automaticIdleDuration + autoTimeOffset)
            {
                Triggered();
            }
        }
    }

    void FixedUpdate()
    {
        // check for player
        if (isLooking && !isAutomatic)
        {
            BoxCollider2D collider = (BoxCollider2D)this.GetComponent<BoxCollider2D>();
            var bounds = collider.bounds;
            //var bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
            //var bottomRight = new Vector2(bounds.max.x, bounds.min.y);
            var incrementX = bounds.size.x/(lookingRaycasts - 1);

            for (int i = 0; i < lookingRaycasts; i++)
            {
                var rayOrigin = new Vector2(bounds.min.x + incrementX * i, bounds.min.y);
                var hit = Physics2D.Raycast(rayOrigin, Vector2.down, 2f, 1 << GameLayers.LAYER_PLAYER);
                Debug.DrawLine(rayOrigin, Vector2.down * 2f + rayOrigin, Color.cyan);
                if (hit)
                {
                    Triggered();
                    //Debug.Log("MO hit " + i + ":" + hit.collider.gameObject.name);
                    break;
                }
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("crushed");
        print(other.gameObject.layer);
        if (other.gameObject.layer == GameLayers.LAYER_PLAYER)
        {
            PlayerHealth.Instance.ReceiveDamage(100);
            isFalling = false;
            isReturning = true;
        }
    }

    private void Triggered()
    {
        isTriggered = true;
        isLooking = false;
        timeTriggered = Time.time;
        shaker.isShaking = true;
    }

    //public float startTargetDirectionY = -1f;
    //public float maxSpeedYDown = 1f;
    //public float maxSpeedYUp = 1f;
    //public float accelerationTimeDown = 0.05f;
    //public float accelerationTimeUp = 0.05f;
    //public float turnDistance = 0.02f;

    //float oldTargetY;
    //public Vector3 velocity;
    //protected float velocityYSmoothing;
    //public float targetDirectionY;

    //override public void Start()
    //{
    //    CalculateRaySpacing();
    //    targetDirectionY = startTargetDirectionY;
    //}
    //// Update is called once per frame
    //void Update() {

    //    if (targetDirectionY < 0) { 
    //        float targetVelocityY = maxSpeedYDown * targetDirectionY;
    //        velocity.y = Mathf.SmoothDamp(velocity.y, targetVelocityY, ref velocityYSmoothing, accelerationTimeDown);
    //    }
    //    else
    //    {
    //        float targetVelocityY = maxSpeedYUp * targetDirectionY;
    //        velocity.y = Mathf.SmoothDamp(velocity.y, targetVelocityY, ref velocityYSmoothing, accelerationTimeUp);
    //    }
    //    oldTargetY = targetDirectionY;
    //    Vector3 displacement = velocity * Time.deltaTime;
    //    VerticalCollisions(ref displacement);
    //    transform.position += displacement;
    //}

    //void OnTriggerEnter2D(Collider2D other)
    //{
    //    print(other.gameObject.layer);
    //    if (other.gameObject.layer == GameLayers.LAYER_PLAYER)
    //    {
    //        PlayerHealth.Instance.ReceiveDamage(100);
    //    }
    //    else if (other.gameObject.layer == GameLayers.LAYER_OBSTACLE)
    //    {
    //        //targetDirectionY = -targetDirectionY;
    //    }
    //}

    //protected virtual void VerticalCollisions(ref Vector3 displacement)
    //{
    //    float directionY = targetDirectionY;
    //    float rayLength = Mathf.Abs(displacement.y) + skinWidth;

    //    for (int i = 0; i < 1; i++)
    //    {
    //        Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
    //        rayOrigin += Vector2.right * verticalRaySpacing * i;
    //        RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, Mathf.Infinity, collisionMask);

    //        Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.red);

    //        if (hit)
    //        {
    //            print(hit.distance);
    //            if (hit.distance < turnDistance)
    //                targetDirectionY = -targetDirectionY;
    //        }
    //    }
    //}
}
