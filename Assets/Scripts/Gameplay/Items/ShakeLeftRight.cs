﻿using UnityEngine;
using System.Collections.Generic;

public class ShakeLeftRight : MonoBehaviour
{
    public bool isShaking = false;
    public float minDuration = 0.3f;
    public float maxDuration = 1.0f;
    public float shakeDistance = 0.1f;

    private float timeLastBob = 0f;
    private float duration = 0f;
    private bool isUp = false;

    void Start()
    {
        duration = Random.Range(minDuration, maxDuration);
    }

    void Update()
    {
        if (!isShaking) return;
        if (Time.time > timeLastBob + duration)
        {
            BobShift();
            isUp = !isUp;
            timeLastBob = Time.time;
            duration = Random.Range(minDuration, maxDuration);
        }
    }

    private void BobShift()
    {
        Vector3 moveDist = new Vector3(shakeDistance, 0, 0)*(isUp ? -1.0f : 1.0f);
        transform.position = transform.position + moveDist;
    }
}
