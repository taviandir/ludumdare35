﻿using UnityEngine;
using System.Collections.Generic;

public class DestructibleGround : MonoBehaviour
{
    public void RemoveGround()
    {
        Destroy(this.gameObject);
        SoundManager.Instance.PlayBreakableGroundDestroyedSound();
    }
}
