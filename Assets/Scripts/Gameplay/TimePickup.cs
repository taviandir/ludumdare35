﻿using UnityEngine;

public class TimePickup : MonoBehaviour {
    public float worth = 2;

    void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.GetComponent<PlatformerPlayer>();
        if (player)
        {
            GameManager.Instance.countdown += worth;
            SoundManager.Instance.PlayTimePickupSound();
            Destroy(this.gameObject);
        }
    }
}
