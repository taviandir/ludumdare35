﻿using UnityEngine;

public class CoinPickup : MonoBehaviour
{
    int worth = 1;
    void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.GetComponent<PlatformerPlayer>();
        if(player)
        {
            GameManager.Instance.coins += worth;
            SoundManager.Instance.PlayCoinPickupSound();
            Destroy(this.gameObject);
        }
    }
}
