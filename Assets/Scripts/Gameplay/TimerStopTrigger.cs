﻿using UnityEngine;
using System.Collections.Generic;

public class TimerStopTrigger : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == GameLayers.LAYER_PLAYER)
        {
            SoundManager.Instance.StopCurrentMusic();
            GameManager.Instance.countdownStarted = false;
        }
    }
}
