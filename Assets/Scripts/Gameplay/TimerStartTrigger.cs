﻿using UnityEngine;
using System.Collections.Generic;

public class TimerStartTrigger : MonoBehaviour
{
    public bool hasTriggered = false;
    public float countDownDuration = 2f;
    //public int initialTime = 30;
    public PlatformerPlayer player;

    private float timeTriggered;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == GameLayers.LAYER_PLAYER)
        {
            hasTriggered = true;
            player.ToggleControl(false);
            timeTriggered = Time.time;
            SoundManager.Instance.StopCurrentMusic();
            SoundManager.Instance.PlayCountdownSound();
        }
    }

    void LateUpdate()
    {
        if (hasTriggered && Time.time > timeTriggered + countDownDuration)
        {
            hasTriggered = false;
            player.ToggleControl(true);
            SoundManager.Instance.PlayRunningMusic();
            //GameManager.Instance.countdown = initialTime;
            GameManager.Instance.countdownStarted = true;
            Destroy(this);
        }
    }
}
