﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class TutorialOverTrigger : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == GameLayers.LAYER_PLAYER)
        {
            SoundManager.Instance.StopCurrentMusic();
            GameManager.Instance.countdownStarted = false;
            SceneManager.LoadScene(0); // back to main menu
        }
    }
}
