﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class EnemyHealth : HealthBase // ToDo
{
    private Enemy enemy;

    void Start()
    {
        enemy = GetComponent<Enemy>();
    }

    protected override void Death()
    {
        enemy.animator.SetTrigger("Die");
        enemy.moveSpeed = 0;
        enemy.GetComponent<BoxCollider2D>().enabled = false;
        StartCoroutine(DelayedDestroy(1.5f));
        var en = GetComponent<Enemy>();
        if (en)
            en.PlayDeathSound();
    }

    protected IEnumerator DelayedDestroy(float delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(gameObject);
    }
}
