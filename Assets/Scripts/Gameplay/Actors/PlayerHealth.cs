﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerHealth : HealthBase
{
    public int startHealth = 5;
    public float invulnerableDuration = 1.5f;
    public List<Image> filledHealthIcons;
    public SpriteRenderer playerSr;

    private static PlayerHealth _instance;

    public static PlayerHealth Instance
    {
        get { return _instance; }
    }

    protected override void Awake()
    {
        //base.Awake();
        _instance = this;
        health = Mathf.Min(startHealth, maxHealth);
    }

    void LateUpdate()
    {
        for (int i = 0; i < filledHealthIcons.Count; ++i)
            filledHealthIcons[i].enabled = i < health;
    } 

    protected override void Death()
    {
        StopCoroutine(DoInvulnerability());
        EventManager.Instance.OnPlayerDeath.Invoke();
        SoundManager.Instance.PlayPlayerDeathSound();
    }

    public override void ReceiveDamage(int damage, bool damageInvulnerable)
    {
        base.ReceiveDamage(damage, damageInvulnerable);
        SoundManager.Instance.PlayPlayerHurtSound();
        if (health > 0)
        {
            isInvulnerable = true;
            StartCoroutine(DoBlinkingPlayer());
            StartCoroutine(DoInvulnerability());
        }
    }

    public void ResetHealth()
    {
        health = maxHealth;
        isDead = false;
    }

    public void MakeVulnerable()
    {
        isInvulnerable = false;
    }

    public void GrantHealth(int amount)
    {
        health += amount;
        health = Mathf.Min(health, maxHealth);
    }

    private IEnumerator DoInvulnerability()
    {
        yield return new WaitForSeconds(invulnerableDuration);
        isInvulnerable = false;
    }

    private IEnumerator DoBlinkingPlayer()
    {
        while (isInvulnerable)
        {
            playerSr.color = new Color(1, 0, 0, 0.5f);
            yield return new WaitForSeconds(0.1f);
            playerSr.color = new Color(1, 1, 1, 0.5f);
            yield return new WaitForSeconds(0.1f);
        }
        playerSr.color = new Color(1, 1, 1, 1);
    }
}
