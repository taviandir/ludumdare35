﻿using UnityEngine;
using System.Collections.Generic;

public abstract class HealthBase : MonoBehaviour
{
    public int maxHealth = 1;
    public bool isInvulnerable = false;
    public bool isDead = false;
    protected int health = 1;

    protected virtual void Awake()
    {
        health = maxHealth;
    }

    public virtual void ReceiveDamage(int damage, bool damageInvulnerable = false)
    {
        if (health <= 0)
            return;
        if (isInvulnerable && !damageInvulnerable)
            return;
        health -= damage;
        if (health < 0)
        {
            health = 0;
        }
        if (health == 0)
            Death();
    }

    protected abstract void Death();
}
