﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ObjectPooler : MonoBehaviour
{
    public int startCount = 4;
    public bool expanding = true;
    public string objectName = "";
    public GameObject poolObject;

    [HideInInspector]
    public List<GameObject> objects;

    void Awake()
    {
        Init();
    }

    public void Init()
    {
        if (poolObject == null) //  || PrefabUtility.GetPrefabType(poolObject) != PrefabType.Prefab
            throw new UnassignedReferenceException("Variable 'poolObject' needs to have a prefab value.");

        // init the list
        bool initialized = false;
        if (objects == null)
            objects = new List<GameObject>(startCount >= 4 ? startCount : 4);
        else
            initialized = true;

        // not yet initialized
        if (!initialized)
        {
            //Debug.Log("first time init");
            // check for pre-created children
            for (int i = 0; i < this.transform.childCount; i++)
            {
                var go = this.transform.GetChild(i).gameObject;
                go.name = objectName + " (Pooled)";
                go.SetActive(false); // make sure they are deactivated
                objects.Add(go);
            }
        }
        else
        {
            //Debug.Log("init since before");
            // check if children are already in the list
            for (int i = 0; i < this.transform.childCount; i++)
            {
                var go = this.transform.GetChild(i).gameObject;
                go.name = objectName + " (Pooled)";
                go.SetActive(false); // make sure they are deactivated
                int goID = go.GetInstanceID();
                bool found = false;

                // check if already added to the objects list
                foreach (var pooledInstance in objects)
                {
                    found = false;
                    int pooledGoID = pooledInstance.GetInstanceID();
                    if (pooledGoID == goID)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    objects.Add(go);
                }
            }
        }

        // create new instances
        if (Application.isPlaying)
            while (objects.Count < startCount)
                CreateAndPoolNewObject();
    }

    public void ClearPooler()
    {
        if (Application.isEditor)
        {
            var children = (from Transform child in transform select child.gameObject).ToList();
            children.ForEach(DestroyImmediate);
            objects.Clear();
        }
        else
            Debug.LogError("ObjectPooler.ClearPooler() may only be called in Edit Mode!");
    }

    public GameObject GetPooledObject()
    {
        foreach (var go in objects)
            if (!go.activeSelf)
                return ActivateAndReturnObject(go); // we activate it right away to make 
                                                    // sure it doesn't get taken by more than one source

        // went through all pooled objects, but all of them were in use
        // Therefor, we need to create another one, if the setting is enabled
        if (expanding)
            return CreateAndPoolNewObject();
        else
            return null; // if we can't, we have to return a null value
    }

    private GameObject ActivateAndReturnObject(GameObject go)
    {
        go.SetActive(true);
        return go;
    }

    private GameObject CreateAndPoolNewObject()
    {
        GameObject go;
        go = (GameObject)Instantiate(poolObject, Vector3.zero, Quaternion.identity);
        go.transform.parent = this.transform;
        go.name = objectName + " (Pooled)";
        go.SetActive(false);
        objects.Add(go);
        return go;
    }
}
