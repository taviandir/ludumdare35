﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(ObjectPooler))]
public class ObjectPoolerEditor : Editor
{
    private ObjectPooler _op;

    void OnEnable()
    {
        _op = (ObjectPooler)target;
    }

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Populate pooler"))
        {
            _op.Init();
            while (_op.objects.Count < _op.startCount)
                CreatePooledObject();
        }
        if (GUILayout.Button("Clear pooler"))
        {
            _op.ClearPooler();
        }
        base.OnInspectorGUI();
    }

    private void CreatePooledObject()
    {
        GameObject go;
        go = (GameObject)PrefabUtility.InstantiatePrefab(_op.poolObject);
        go.transform.parent = _op.transform;
        go.name = _op.objectName + " (Pooled)";
        go.SetActive(false);
        _op.objects.Add(go);
    }
}
