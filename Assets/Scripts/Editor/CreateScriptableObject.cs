﻿using System;
using System.IO;
using UnityEngine;
using UnityEditor;

public class CreateScriptableObject
{
    public static void InitAsset(string filename, bool overwriteIfExists = false)
    {
        if (string.IsNullOrEmpty(filename))
        {
            Debug.LogWarning("EDITOR: Parameter 'filename' was null");
            return;
        }

        var asset = ScriptableObject.CreateInstance<MyScriptableObjectClass>();
        string filePath = String.Format("Assets/{0}.asset", filename);

        // create and save new object
        if (File.Exists(filePath) && !overwriteIfExists)
        {
            Debug.LogWarning("FILE at that spot already exists! No object created");
            return;
        }
        if (File.Exists(filePath) && !overwriteIfExists)
        {
            Debug.LogWarning("FILE at that spot already exists! Overwriting existing file...");
        }
        AssetDatabase.CreateAsset(asset, filePath);
        AssetDatabase.SaveAssets();

        // focus new asset
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
}