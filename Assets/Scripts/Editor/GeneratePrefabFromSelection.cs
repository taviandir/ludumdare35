/* 
 * Created:         2013-01-01
 * Last updated:    2013-01-01
 * Description:     
 * Comment:         
 * 
 ************************************
 *
 * Imports:
 * 
 */

using UnityEditor;
using UnityEngine;
using System.Linq;
using System.Collections;
using System.IO;
using System;

public class GeneratePrefabFromSelection : MonoBehaviour 
{
    [MenuItem("Project Tools/Make prefab from selection %e")]
    static void MakePrefabFromSelectionMenuItem()
    {
        GameObject[] selected = Selection.gameObjects;
        string path = "Assets/Prefabs/";
        string filename;
        string fullPath;
        Directory.CreateDirectory(path);

        foreach (GameObject go in selected)
        {
            filename = go.name;
            fullPath = path + filename + ".prefab";

            int counter = 1;
            while (File.Exists(fullPath))
            {
                fullPath = path + filename + " (" + counter++ + ")" + ".prefab";
            }

            CreateNew(go, fullPath);
        }
        AssetDatabase.Refresh();
    }

    [MenuItem("Project Tools/Make prefab with selection %e", true)]
    static bool ValidateMakePrefabMenuITem()
    {
        return Selection.activeGameObject != null;
    }

    private static void CreateNew(GameObject go, string path)
    {
        var prefab = PrefabUtility.CreateEmptyPrefab(path);
        PrefabUtility.ReplacePrefab(go, prefab, ReplacePrefabOptions.ConnectToPrefab);
    }
}
