﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.IO;

public class TodoListWindow : EditorWindow 
{
	private const int width = 400;
	private const int height = 600;
	private const int buttonFrameHeight = 20;
	
	private const string filePath = "Assets/ScriptableObjects/ToDoList.asset";
	private static string text = null;
	
	[MenuItem("Project Tools/ToDo List", false, 1)]
	public static void Init()
	{
		var window = GetWindow<TodoListWindow>();
        window.position = new Rect(Screen.width / 2 - 100, Screen.height / 2 - 20, width, height); // 80 seems to be the minimum height
        window.titleContent.text = "Project ToDo";
		LoadText();
        window.Show();
	}
	
	void OnGUI()
	{
		try
		{
			GUILayout.BeginArea(new Rect(0, 0, width, buttonFrameHeight));
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Save and close"))
			{
				SaveAndClose();
			}
			if (GUILayout.Button("Cancel"))
			{
				Cancel();
			}
			GUILayout.EndHorizontal();
			GUILayout.EndArea();
			
			// text field
			GUILayout.BeginArea(new Rect(0, buttonFrameHeight, width, height - buttonFrameHeight));
			EditorGUI.BeginChangeCheck();
			text = GUILayout.TextArea(text, GUILayout.Height(height - buttonFrameHeight));
			if (EditorGUI.EndChangeCheck())
			{
				SaveAsset(text);
			}
			GUILayout.EndArea();
		}
		catch
		{}
	}
	
	void OnLostFocus()
	{
		CloseWindow();
	}
	
	private void SaveAndClose()
	{
		// Create dir if not exists
		string projectPath = Application.dataPath + "/";
		string dirPath = projectPath + "ScriptableObjects";
		Directory.CreateDirectory(dirPath);
		
		// save and close
		SaveAsset(text, true);
		CloseWindow();
	}
	
	private static void LoadText()
	{
		// ToDo
		if (File.Exists(filePath))
		{
			var asset = AssetDatabase.LoadAssetAtPath<ToDoListSO>(filePath);
			text = asset.text;
		}
		else
		{
			text = "";
		}
	}
	
	private void Cancel()
	{
		CloseWindow();
	}
	
	private void CloseWindow()
	{
		text = null;
		var window = GetWindow<TodoListWindow>();
		window.Close();
	}
	
	public static ToDoListSO SaveAsset(string data, bool overwriteIfExists = false)
    {	
        ToDoListSO asset = ScriptableObject.CreateInstance<ToDoListSO>();
		asset.text = data;

        // create and save new object
        if (File.Exists(filePath) && !overwriteIfExists)
        {
            //  Debug.LogWarning("FILE at that spot already exists! No object created");
            return null;
        }
        if (File.Exists(filePath) && overwriteIfExists)
        {
            //  Debug.LogWarning("FILE at that spot already exists! Overwriting existing file...");
        }
        AssetDatabase.CreateAsset(asset, filePath);
        AssetDatabase.SaveAssets();

        return asset;
    }
}