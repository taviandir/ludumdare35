﻿using UnityEditor;
using UnityEngine;

public class TileMapCreatorColorsWindow : EditorWindow
{
    private TileMapCreator _tileMapCreator;

    public void Init(TileMapCreator _tileMapCreator)
    {
        this._tileMapCreator = _tileMapCreator;

        // Get existing open window or if none, make a new one:
        TileMapCreatorColorsWindow window = (TileMapCreatorColorsWindow)EditorWindow.GetWindow(typeof(TileMapCreatorColorsWindow));
        window.Show();
        this.titleContent = new GUIContent("Choose colors");
    }

    public void OnGUI()
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Grid color", GUILayout.Width(100));
        _tileMapCreator.gridColor = EditorGUILayout.ColorField(_tileMapCreator.gridColor);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Connection color", GUILayout.Width(100));
        _tileMapCreator.connectionColor = EditorGUILayout.ColorField(_tileMapCreator.connectionColor);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Can place color", GUILayout.Width(100));
        _tileMapCreator.canPlaceColor = EditorGUILayout.ColorField(_tileMapCreator.canPlaceColor);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Cannot place color", GUILayout.Width(100));
        _tileMapCreator.cannotPlaceColor = EditorGUILayout.ColorField(_tileMapCreator.cannotPlaceColor);
        EditorGUILayout.EndHorizontal();
    }
}
