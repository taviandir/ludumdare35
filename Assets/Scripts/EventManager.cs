﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class EventManager : MonoBehaviour
{
    public UnityEvent OnPlayerDeath;
    public UnityEvent OnPlayerRespawn;
    public UnityEvent OnTimeOver; // need to GameManager.Instance.ResetCountdown();

    private static EventManager _instance;
    public static EventManager Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        _instance = this;
    }

    public void RespawnIn(float seconds)
    {
        StartCoroutine(DoRespawn(seconds));
    }

    IEnumerator DoRespawn(float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        OnPlayerRespawn.Invoke();
    }

    public void NewGame(float seconds)
    {
        StartCoroutine(DoNewGame(seconds));
    }

    IEnumerator DoNewGame(float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
