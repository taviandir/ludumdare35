﻿using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public float timeAtStart;
    public bool countdownStarted;
    public float countdown;
    private static GameManager _instance;

    public Text coinText;
    public Text timeText;

    public int coins;

    public static GameManager Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        _instance = this;
    }
    void Start()
    {
        countdown = timeAtStart;
        UpdateTime();
        coinText.text = "" + coins;
    }

    void FixedUpdate()
    {
        if (countdownStarted) { 
            countdown = countdown - Time.fixedDeltaTime;
        }
        if (countdown <= 0)
        {
            countdownStarted = false;
            countdown = 0;
            EventManager.Instance.OnTimeOver.Invoke();
        }
        UpdateTime();
        coinText.text = "" + coins;
        
        
    }

    void UpdateTime()
    {
        int minutes = Mathf.FloorToInt(countdown / 60F);
        int seconds = Mathf.FloorToInt(countdown - minutes * 60);
        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    void ResetCountdown()
    {
        countdown = timeAtStart;
        countdownStarted = false;
    }
}