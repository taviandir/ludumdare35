﻿using UnityEngine;
using System.Collections.Generic;

public static class GameLayers
{
    public static readonly int LAYER_OBSTACLE = 8;
    public static readonly int LAYER_PLAYER = 9;
    public static readonly int LAYER_ENEMY = 10;
    public static readonly int LAYER_PLAYERPROJECTILE = 11;
    public static readonly int LAYER_ENEMYPROJECTILE = 12;
    public static readonly int LAYER_PICKUP = 13;
}
