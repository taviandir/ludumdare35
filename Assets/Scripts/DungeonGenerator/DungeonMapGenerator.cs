﻿/*

Based on the tutorial on Unity3D's official tutorial series:
URL: http://unity3d.com/learn/tutorials/projects/procedural-cave-generation-tutorial

*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DungeonMapGenerator : MonoBehaviour 
{
	[Range(0,100)]
	public int randomGroundPercent;
	public int width;
	public int height;
	public string seed;
	public bool useRandomSeed;

	private int [,] map;

	void Start()
	{
		GenerateMap();
	}

	void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			GenerateMap();
		}
	}

	private void GenerateMap()
	{
		map = new int[width, height];
		RandomFillMap();

		for (int i = 0; i < 5; i++)
		{
			SmoothMap();
		}

		ProcessMap();

		int borderSize = 8;
		int[,] borderedMap = new int[width + borderSize * 2, height + borderSize * 2];

		for (int x = 0; x < borderedMap.GetLength(0); x++)
			for (int y = 0; y < borderedMap.GetLength(1); y++)
			{
				if (x >= borderSize && x < width + borderSize && y >= borderSize && y < height + borderSize) // inside the border
				{
					borderedMap[x,y] = map[x-borderSize, y-borderSize];
				}
				else // the border
				{
					borderedMap[x,y] = 1;
				}
			}

		var meshGen = GetComponent<DungeonMeshGenerator>();
		meshGen.GenerateMesh(borderedMap, 1f);
	}

	private bool IsInMapRange(int x, int y)
	{
		return x >= 0 && x < width && y >= 0 && y < height;
	}

	private void ProcessMap()
	{
		List<List<Coord>> wallRegions = GetRegions(1);

		int wallThreshHoldSize = 50;
		foreach (List<Coord> wallRegion in wallRegions)
			if (wallRegion.Count < wallThreshHoldSize)
				foreach (Coord tile in wallRegion)
					map[tile.tileX,tile.tileY] = 0;

		List<List<Coord>> roomRegions = GetRegions(0);
		int roomThreshHoldSize = 50;
		List<Room> survivingRooms = new List<Room>();

		foreach (List<Coord> roomRegion in roomRegions)
			if (roomRegion.Count < roomThreshHoldSize)
			{
				foreach (Coord tile in roomRegion)
					map[tile.tileX,tile.tileY] = 1;
			}
			else
			{
				survivingRooms.Add(new Room(roomRegion, map));
			}

		survivingRooms.Sort();
//		foreach (Room r in survivingRooms)
//			print (r.roomSize);
		survivingRooms[0].isAccessibleFromMainRoom = true;
		survivingRooms[0].isMainRoom = true;
		ConnectClosestRooms(survivingRooms);
	}

	private void ConnectClosestRooms(List<Room> allRooms, bool forceAccessibilityToMainRoom = false)
	{
		List<Room> roomListA = new List<Room>(); // rooms NOT connected to the main room
		List<Room> roomListB = new List<Room>(); // rooms connected to the main room

		if (forceAccessibilityToMainRoom)
		{
			foreach (Room room in allRooms)
			{
				if (room.isAccessibleFromMainRoom)
					roomListB.Add(room);
				else
					roomListA.Add(room);
			}
		}
		else
		{
			roomListA = allRooms;
			roomListB = allRooms;
		}

		int bestDistance = 0;
		Coord bestTileA = new Coord();
		Coord bestTileB = new Coord();
		Room bestRoomA = null;
		Room bestRoomB = null;
		bool possibleConnectionFound = false;

		foreach (Room roomA in roomListA)
		{
			if (!forceAccessibilityToMainRoom)
			{
				possibleConnectionFound = false;
				if (roomA.connectedRooms.Count > 0)
				{
					continue;
				}
			}

			foreach(Room roomB in roomListB)
			{
				if (roomA == roomB || roomA.IsConnected(roomB))
					continue;
//				if (roomA.IsConnected(roomB))
//			    {
//					possibleConnectionFound = false;
//					break;
//				}

				for (int tileIndexA = 0; tileIndexA < roomA.edgeTiles.Count; tileIndexA++)
					for (int tileIndexB = 0; tileIndexB < roomB.edgeTiles.Count; tileIndexB++)
					{
						Coord tileA = roomA.edgeTiles[tileIndexA];
						Coord tileB = roomB.edgeTiles[tileIndexB];
						int distanceBetweenRooms = (int)(Mathf.Pow(tileA.tileX-tileB.tileX,2) + Mathf.Pow(tileA.tileY-tileB.tileY,2));
						
						if (distanceBetweenRooms < bestDistance || !possibleConnectionFound)
						{
							bestDistance = distanceBetweenRooms;
							possibleConnectionFound = true;
							bestTileA = tileA;
							bestTileB = tileB;
							bestRoomA = roomA;
							bestRoomB = roomB;
						}
					}
			}

			if (possibleConnectionFound && !forceAccessibilityToMainRoom)
			{
				CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB, Color.green);
			}
		}

		if (possibleConnectionFound && forceAccessibilityToMainRoom)
		{
			CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB, Color.cyan);
			ConnectClosestRooms(allRooms, true);
		}

		if (!forceAccessibilityToMainRoom)
			ConnectClosestRooms(allRooms, true);
	}

	private void CreatePassage(Room roomA, Room roomB, Coord tileA, Coord tileB, Color debugLineColor)
	{
		Room.ConnectRooms(roomA,roomB);
		//  Debug.DrawLine(CoordToWorldPoint(tileA), CoordToWorldPoint(tileB), debugLineColor, 100f);
		
		List<Coord> line = GetPassageLine(tileA, tileB);
		foreach(Coord c in line)
			DrawCircle(c, 1);
	}
	
	private void DrawCircle(Coord c, int r)
	{
		for (int x = -r; x <= r; x++)
			for (int y = -r; y <= r; y++)
			{
				if (x*x + y*y <= r*r)
				{
					int drawX = c.tileX + x;
					int drawY = c.tileY + y;
					if (IsInMapRange(drawX, drawY))
					{
						map[drawX,drawY] = 0;
					}
				}
			}
	}
	
	private List<Coord> GetPassageLine(Coord from, Coord to)
	{
		List<Coord> line = new List<Coord> ();

        int x = from.tileX;
        int y = from.tileY;

        int dx = to.tileX - from.tileX;
        int dy = to.tileY - from.tileY;

        bool inverted = false;
        int step = Math.Sign (dx);
        int gradientStep = Math.Sign (dy);

        int longest = Mathf.Abs (dx);
        int shortest = Mathf.Abs (dy);

        if (longest < shortest) {
            inverted = true;
            longest = Mathf.Abs(dy);
            shortest = Mathf.Abs(dx);

            step = Math.Sign (dy);
            gradientStep = Math.Sign (dx);
        }

        int gradientAccumulation = longest / 2;
        for (int i =0; i < longest; i ++) {
            line.Add(new Coord(x,y));

            if (inverted) 
                y += step;
            else 
                x += step;

            gradientAccumulation += shortest;
            if (gradientAccumulation >= longest) 
			{
                if (inverted) 
                    x += gradientStep;
                else 
                    y += gradientStep;
                gradientAccumulation -= longest;
            }
        }

        return line;
	}

	private Vector3 CoordToWorldPoint(Coord tile)
	{
		return new Vector3(-width / 2 + .5f + tile.tileX, 2, -height / 2 + .5f + tile.tileY);
	}

	private List<List<Coord>> GetRegions(int tileType)
	{
		List<List<Coord>> regions = new List<List<Coord>>();
		int[,] mapFlags = new int[width, height];

		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
		{
			if (mapFlags[x,y] == 0 && map[x,y] == tileType)
			{
				List<Coord> newRegion = GetRegionTiles(x,y);
				regions.Add(newRegion);

				foreach(Coord tile in newRegion)
				{
					mapFlags[tile.tileX, tile.tileY] = 1;
				}
			}
		}

		return regions;
	}

	private List<Coord> GetRegionTiles(int startX, int startY)
	{
		List<Coord> tiles = new List<Coord>();
		int[,] mapFlags = new int[width, height];
		int tileType = map[startX, startY];

		Queue<Coord> queue = new Queue<Coord>();
		queue.Enqueue(new Coord(startX, startY));
		mapFlags[startX, startY] = 1;

		while (queue.Count > 0)
		{
			Coord tile = queue.Dequeue();
			tiles.Add(tile);

			for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++)
				for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++)
				{
					if (IsInMapRange(x,y) && (y == tile.tileY || x == tile.tileX))
					{
						if (mapFlags[x,y] == 0 && map[x,y] == tileType)
						{
							mapFlags[x,y] = 1;
							queue.Enqueue(new Coord(x, y));
						}
					}
				}
		}

		return tiles;
	}

	private void RandomFillMap()
	{
		if (useRandomSeed)
		{
			seed = Time.time.ToString();
		}

		System.Random rnd = new System.Random(seed.GetHashCode());

		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
			{
				if (x == 0 || y == 0 || x == width-1 || y == height-1)
					map[x,y] = 1; // wall
				else
					map[x,y] = (rnd.Next(0, 100) < randomGroundPercent) ? 0 : 1;
			} 
	}

	void SmoothMap() 
	{
        for (int x = 0; x < width; x ++) {
            for (int y = 0; y < height; y ++) {
                int neighbourWallTiles = GetSurroundingWallCount(x,y);

                if (neighbourWallTiles > 4)
                    map[x,y] = 1;
                else if (neighbourWallTiles < 4)
                    map[x,y] = 0;

            }
        }
    }

	private int GetSurroundingWallCount(int gridX, int gridY) 
	{
        int wallCount = 0;
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX ++) 
        {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY ++) 
            {
                if (IsInMapRange(neighbourX, neighbourY)) 
                {
                    if (neighbourX != gridX || neighbourY != gridY) 
                    {
                        wallCount += map[neighbourX,neighbourY];
                    }
                }
                else 
                {
                    wallCount ++;
                }
            }
        }

        return wallCount;
    }

	struct Coord
	{
		public int tileX;
		public int tileY;

		public Coord(int x, int y)
		{
			tileX = x;
			tileY = y;
		}
	}

	class Room : IComparable<Room>
	{
		public List<Coord> tiles;
		public List<Coord> edgeTiles;
		public List<Room> connectedRooms;
		public int roomSize;
		public bool isAccessibleFromMainRoom = false;
		public bool isMainRoom = false;

		public Room()
		{
		}

		public Room(List<Coord> roomTiles, int[,] map)
		{
			tiles = roomTiles;
			roomSize = roomTiles.Count;
			connectedRooms = new List<Room>();
			edgeTiles = new List<Coord>();

			foreach(Coord tile in tiles)
				for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++)
					for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++)
						if (x == tile.tileX || y == tile.tileY)
							if (map[x,y] == 1) // is wall tile?
								edgeTiles.Add(tile);

		}

		public bool IsConnected(Room otherRoom)
		{
			return connectedRooms.Contains(otherRoom);
		}

		public void SetAccessibleFromMainRoom()
		{
			if (!isAccessibleFromMainRoom)
			{
				isAccessibleFromMainRoom = true;
				foreach(Room connectedRoom in connectedRooms)
					connectedRoom.SetAccessibleFromMainRoom();
			}
		}

		public static void ConnectRooms(Room roomA, Room roomB)
		{
			if (roomA.isAccessibleFromMainRoom)
				roomB.SetAccessibleFromMainRoom();
			else if (roomB.isAccessibleFromMainRoom)
				roomA.SetAccessibleFromMainRoom();

			roomA.connectedRooms.Add(roomB);
			roomB.connectedRooms.Add(roomA);
		}

		public int CompareTo(Room otherRoom)
		{
			return otherRoom.roomSize.CompareTo(roomSize);
		}
	}

	void OnDrawGizmos()
	{
		return;
//		if (map != null)
//			for (int x = 0; x < width; x++)
//				for (int y = 0; y < height; y++)
//				{
//					Gizmos.color = (map[x,y] == 1) ? Color.black : Color.white;
//					Vector3 pos = new Vector3(-width/2 + x + .5f, -height/2 + y + .5f, 0);
//					Gizmos.DrawCube(pos, Vector3.one);
//				}	
	}
}